﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public class Spawner
    {
        private float scale;
        private float speed = 50.0f;
        private float actualSpeed;
        private Ingredient ingredient;
        private Character player;
        private float timeToSpawn = 2.0f;
        private float currentTimeToSpawn = 0.0f;
        private Vector2 position;
        public Vector2 Position { get => position; set => position = value; }
        private float walls = 500.0f;

        public Spawner(Vector2 position, float scale, Character player)
        {
            Initialize(position, scale, player);
        }

        public void Initialize(Vector2 position, float scale, Character player)
        {
            actualSpeed = speed;
            this.position = position;
            this.scale = scale;
            this.player = player;
            currentTimeToSpawn = timeToSpawn;
        }

        public void Update()
        {
            if (ingredient != null && !ingredient.isPickedUp) ingredient.Update();

            CheckSpawnTimer();
            CheckMovement();
        }

        public void Render()
        {
            if (ingredient != null && !ingredient.isPickedUp) ingredient.Render();
        }

        private void CheckSpawnTimer()
        {
            currentTimeToSpawn += Time.deltaTime;
            if (currentTimeToSpawn >= timeToSpawn) 
                SpawnItem();
        }

        private void SpawnItem()
        {
            var randomItem = new Random();
            var randomIngredient = randomItem.Next(1, 4);

            if (randomIngredient == 1)
            {
                ingredient = new Ingredient(new Vector2(position.X, position.Y), "Art/tomatoe.png", 0.25f, 0f, player);
                currentTimeToSpawn = 0.0f;
                Engine.Debug("tomatoe");
            }

            else if (randomIngredient == 2)
            {
                ingredient = new Ingredient(new Vector2(position.X, position.Y), "Art/carrot.png", 0.25f, 0f, player);
                currentTimeToSpawn = 0.0f;
                Engine.Debug("zanahoria");
            }

            else if (randomIngredient == 3)
            {
                ingredient = new Ingredient(new Vector2(position.X, position.Y), "Art/sausage.png", 0.15f, 0f, player);
                currentTimeToSpawn = 0.0f;
                Engine.Debug("salchicha");
            }

            else
                Engine.Debug("");
        }

        private void CheckMovement()
        {
            position.X += actualSpeed * Time.deltaTime;
            if (position.X >= walls) actualSpeed = -speed;
            else if (position.X <= 0f) actualSpeed = speed;

            Engine.Debug($"la posición es {position.X}");
        }
    }
}

            