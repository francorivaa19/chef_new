﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public class MainMenu
    {
        private Button playButton;
        private Button creditsButton;
        private Button selectedButton;

        private string backgroundTexturePath;
        private float inputDelayTime = 0.2f;
        private float currentInputDelayTime;

        public MainMenu()
        {
            Initialize(backgroundTexturePath);

            playButton = new Button(new Vector2(120f, 250f), 10f, "Art/Buttons/PlayButton.png", "Art/Buttons/Play Button HIGHLITED.png");
            creditsButton = new Button(new Vector2(120f, 350f), 0.25f, "Art/Buttons/Exit Button Normal.png", "Art/Buttons/Exit Button Highlited.png");

            playButton.AssignButton(creditsButton, creditsButton);
            creditsButton.AssignButton(playButton, playButton);

            SelectButton(playButton);
        }

        public void Initialize(string texturePath)
        {
            backgroundTexturePath = texturePath;
        }

        public void Update()
        {
            currentInputDelayTime += Time.deltaTime;

            if (Engine.GetKey(Keys.DOWN) && currentInputDelayTime > inputDelayTime)
            {
                currentInputDelayTime = 0f;
                SelectButton(selectedButton.NextButton);
            }

            if (Engine.GetKey(Keys.UP) && currentInputDelayTime > inputDelayTime)
            {
                currentInputDelayTime = 0f;
                SelectButton(selectedButton.PreviousButton);
            }

            if (Engine.GetKey(Keys.SPACE) && currentInputDelayTime > inputDelayTime)
            {
                currentInputDelayTime = 0f;
                PressSelectedButton();
            }
        }

        public void Render()
        {
            Engine.Draw(backgroundTexturePath);

            playButton.Render();
            creditsButton.Render();
        }

        public void SelectButton(Button button)
        {
            if (selectedButton != null)
            {
                selectedButton.Unselect();
            }
            
            selectedButton = button;
            selectedButton.Select();
        }

        private void PressSelectedButton()
        {
            if (selectedButton != null)
            {
                if (selectedButton == playButton)
                {
                    GameManager.Instance.ChangeState(State.Level);
                }

                else if (selectedButton == creditsButton)
                {

                }
            }
        }
    }
}
