﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public delegate void PointsToDelegate(int param1, int param2);

    public class PointsManager
    {
        public event PointsToDelegate OnPointsChange;
        public int MaxPoints { get; private set; }
        public int CurrentPoints { get; private set; }

        public PointsManager(int maxPoints)
        {
            MaxPoints = maxPoints;
            CurrentPoints = maxPoints;
        }

        public void AddPoints(int points)
        {
            CurrentPoints += points;
            OnPointsChange?.Invoke(CurrentPoints, points);
        }
    }
}
