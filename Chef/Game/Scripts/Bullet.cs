﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Game.Scripts
//{
//    public class Bullet
//    {
//        #region fields
//        private float speed = 100f;
//        private float scale = 1f;
//        private float angle = 0f;
//        private float x;
//        private float y;
//        private string texture;
//        private Texture bulletTexture;
//        private float RealWidth => bulletTexture.Width;
//        private float RealHeight => bulletTexture.Height * scale;
//        #endregion fields

//        public Bullet(float initialX, float initialY, string texture, float scale, float angle)
//        {
//            x = initialX;
//            y = initialY;
//            this.scale = scale;
//            this.angle = angle;
//            this.texture = texture;

//            bulletTexture = Engine.GetTexture(texture);
//        }

//        public void Update()
//        {
//            x += speed * Time.deltaTime;
//            Render();
//        }

//        public void Render()
//        {
//            Engine.Draw(bulletTexture, x, y, scale, scale, angle, RealHeight / 2f, RealWidth / 2f);
//        }
//    }
//}
