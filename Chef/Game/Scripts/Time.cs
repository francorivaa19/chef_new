﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public static class Time
    {
        #region fields
        public static DateTime startDate;
        public static float lastFrame;
        public static float deltaTime;
        #endregion fields

        public static void Start()
        {
            startDate = DateTime.Now;
        }

        public static void CalculateDeltaTime()
        {
            float currentTime = (float) (DateTime.Now - startDate).TotalSeconds;
            deltaTime = currentTime - lastFrame;

            lastFrame = currentTime;
        }
    }
}
