﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public enum State
    {
        MainMenu, Level, GameOver, Victory
    }

    public class GameManager 
    {
        private static GameManager instance;
        private MainMenu mainMenu;
        private Endscreen victoryScreen;
        private State currentState;
        public static Level level;

        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameManager();
                }

                return instance;
            }
        }

        private GameManager()
        {
            Initialize();
        }

        public void Initialize()
        {
            level = new Level();
            mainMenu = new MainMenu();
            victoryScreen = new Endscreen("Art/Screens/kitchenWin.png", 2.0f);
            //gameOverScreen = new Endscreen("Art/Screens/kitchenLose.png", 2.0f);

            ChangeState(State.MainMenu);
        }

        public void Update()
        {
            switch (currentState)
            {
                case State.MainMenu:
                    mainMenu.Initialize("Art/Screens/ChefMainMenu.png");
                    mainMenu.Update();
                    break;

                case State.Level:
                    level.Awake();
                    level.Start();
                    level.Update();
                    break;

                case State.GameOver:
                    break;

                case State.Victory:
                    victoryScreen.Update();
                    break;
            }
        }

        public void Render()
        {
            switch (currentState)
            {
                case State.MainMenu:
                    mainMenu.Render();
                    break;

                case State.Level:
                    level.Render();
                    break;

                case State.GameOver:

                    break;

                case State.Victory:
                    victoryScreen.Render();
                    break;
            }
        }

        public void ChangeState(State state)
        {
            currentState = state;
        }
    }
}
