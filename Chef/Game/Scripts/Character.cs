﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public delegate void TestDel();

    public class Character
    {
        #region fields

        private float RealWidth => characterTexture.Width * scale;
        private float RealHeight => characterTexture.Height * scale;

        private Vector2 position;
        public Vector2 Position { get => position; set => position = value; }
        
        private float speed = 150.0f;
        private float scale = 0.5f;
        private float angle;
        private float timeToShoot = 1f;
        private float currentTimeToShoot = 0.0f;
        private string texture;
        private Texture characterTexture;
        private bool canMove = true;
        private float wallsHeight = 500f;
        //private List<Bullet> bulletList;

        public event TestDel OnPlayerIsDead;

        //private event TestDel OnPlayerIsDead
        //{
        //    add
        //    {
        //        OnPlayerIsDead += value;
        //    }
        //    remove
        //    {
        //        OnPlayerIsDead -= value;
        //    }
        //}

        public Vector2 Size => new Vector2(RealWidth, RealHeight);
        public Vector2 Center => new Vector2(position.X + RealWidth / 2f, position.Y + RealHeight / 2f);
        public float Radius => RealHeight > RealWidth ? RealHeight / 2f : RealWidth / 2f;
        #endregion fields

        /*Constructor es el método de inicialización*/

        public Character (Vector2 initialPosition, string texture, float scale, float angle)
        {
            this.position = initialPosition;
            this.scale = scale;
            this.angle = angle;           
            this.texture = texture;
            characterTexture = Engine.GetTexture(texture);
        }

        public void Awake()
        {           
            //bulletList = new List<Bullet>();
        }

        public void Start()
        {
            currentTimeToShoot = timeToShoot;
        }

        public void Update()
        {
            CheckAttack();
            CheckMovement();
        }

        private void CheckMovement()
        {
            if (canMove)
            {
                if (Engine.GetKey(Keys.A))
                    this.position.X -= speed * Time.deltaTime;

                if (Engine.GetKey(Keys.D))
                    this.position.X += speed * Time.deltaTime;
            }

            #region wallCollision

            if (position.X >= wallsHeight)
            {
                canMove = false;
                speed = 0.0f;

                if (Engine.GetKey(Keys.A))
                {
                    canMove = true;
                    speed = 150.0f;
                }

                else if (Engine.GetKey(Keys.D))
                    canMove = false;
            }

            if (position.X <= 0f)
            {
                canMove = false;
                speed = 0.0f;

                if (Engine.GetKey(Keys.D))
                {
                    canMove = true;
                    speed = 100.0f;
                }
                else if (Engine.GetKey(Keys.A))
                    canMove = false;
            }

            #endregion wallCollision
        }

        private void CheckAttack()
        {
            if (Engine.GetKey(Keys.K))
            {
                //OnPlayerIsDead();
                //if (OnPlayerIsDead != null) OnPlayerIsDead.Invoke();

                OnPlayerIsDead?.Invoke();
            } 

            //if (Engine.GetKey(Keys.SPACE)) Attack();
        }

        private void Attack()
        {
            currentTimeToShoot += Time.deltaTime;

            if (currentTimeToShoot >= timeToShoot)
            {
                //Bullet bullet = new Bullet(X, y, "Art/shuriken.png", 0.1f, 0f);
                //bulletList.Add(bullet);

                currentTimeToShoot = 0.0f;
            }
        }

        public void Render()
        {
            //for (int i = 0; i < bulletList.Count; i++) bulletList[i].Update();
            Engine.Draw(characterTexture, this.position.X, this.position.Y, scale, scale, 0f, RealWidth / 2f, RealHeight / 2f);
        }
    }
}
