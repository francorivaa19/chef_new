﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public class Endscreen
    {
        public string texture;
        public float timeToBack = 2.0f;
        public float currentTimeToBack;

        public Endscreen(string texture, float timeToBack)
        {
            Initialize(texture, timeToBack);
        }

        public void Initialize(string texture, float timeToBack)
        {
            this.texture = texture;
            this.timeToBack = timeToBack;
        }

        public void Update()
        {
            currentTimeToBack += Time.deltaTime;
            if(currentTimeToBack >= timeToBack)
            {
                currentTimeToBack = 0.0f;
                GameManager.Instance.ChangeState(State.MainMenu);
            }
        }

        public void Render()
        {
            Engine.Draw(texture);
        }
    }
}
