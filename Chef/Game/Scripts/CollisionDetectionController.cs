﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public static class CollisionDetectionController
    {
        public static bool DetectBoxCollider(Vector2 playerPos, Vector2 playerSize, Vector2 ingredientPos, Vector2 ingredientSize)
        {
            float distanceX = Math.Abs(playerPos.X - ingredientPos.X);
            float distanceY = Math.Abs(playerPos.Y - ingredientPos.Y);
        
            float halfWidthSum = playerSize.X / 2f + ingredientSize.X / 2f;
            float halfHeightSum = playerSize.Y / 2f + ingredientSize.Y / 2f;
        
            return distanceX <= halfWidthSum && distanceY <= halfHeightSum;
        }

        public static bool DetectCircleCollider(Vector2 playerPos, float playerRadius, Vector2 ingredientPos, float ingredientRadius)
        {
            float distanceX = playerPos.X - ingredientPos.X;
            float distanceY = playerPos.Y - ingredientPos.Y;

            //es el cuadrado
            float totalDistance = (float) Math.Sqrt(distanceX * distanceX + distanceY * distanceY);
            return totalDistance < playerRadius + ingredientRadius;
        }

        public static bool DetectCircleWithBoxColliding(Vector2 playerPos, Vector2 playerSize, Vector2 ingredientPos, float ingredientRadius)
        {
            float positionX = ingredientPos.X;
            if (positionX < playerPos.X) positionX = playerPos.X;
            if (positionX > playerPos.X + playerSize.X) positionX = playerPos.X + playerSize.X;

            float positionY = ingredientPos.Y;
            if (positionY < playerPos.Y) positionY = playerPos.Y;
            if (positionY > playerPos.Y + playerSize.Y) positionY = playerPos.Y + playerSize.Y;

            float distance = (float)Math.Sqrt((ingredientPos.X - positionX) * (ingredientPos.X - positionX)
                + (ingredientPos.Y - positionY) * (ingredientPos.Y - positionY));

            return distance < ingredientRadius;
        }
    }
}
