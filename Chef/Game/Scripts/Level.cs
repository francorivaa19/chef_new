﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public class Level
    {
        private Background background;
        private Character player;
        public Character Player { get => player; set => player = value; }

        //private PointsManager points;
        //private Ingredient ingredient;
        private Spawner spawner;

        private float timeToSpawn = 2.0f;
        private float currentTimeToSpawn = 0.0f;

        private int ingredientsAmount = 0;

        public Level()
        {
            Initialize();
        }

        public void Initialize()
        {
            player = new Character(new Vector2(250f, 650f), "Art/chef.png", 0.5f, 0f);
            background = new Background(300f, 520f, "Art/kitchen.png", 1f, 0f);
            spawner = new Spawner(new Vector2(250f, 250f), 1f, player);

            //points = new PointsManager(10);

            Player.OnPlayerIsDead += OnPlayerIsDeadHandler;
            //points.OnPointsChange += OnChangePointsHandler;

            currentTimeToSpawn = timeToSpawn;
        }

        public void Awake()
        {
            player.Awake();
        }

        public void Start()
        {
            player.Start();
        }

        public void Update()
        {
            background.Update();
            player.Update();
            spawner.Update();

            CheckSpawnTime();
            CheckPoints();
        }

        public void Render()
        {
            background.Render();
            player.Render();
            spawner.Render();
        }

        public void CheckSpawnTime()
        {
            currentTimeToSpawn += Time.deltaTime;
            if (currentTimeToSpawn >= timeToSpawn)
            {
                currentTimeToSpawn = 0.0f;
            }
        }

        public void CheckPoints()
        {
            if (ingredientsAmount == 5)
            {
                GameManager.Instance.ChangeState(State.Victory);
            }
        }

        //private void AddPoint()
        //{
        //    currentTimeToAddPoint += Time.deltaTime;
        //    if (currentTimeToAddPoint >= timeToAddPoint)
        //    {
        //        ingredientsAmount++;
        //        currentTimeToAddPoint = 0.0f;
        //    }
        //}

        private void OnPlayerIsDeadHandler()
        {
            GameManager.Instance.ChangeState(State.GameOver);
        }

        public static bool DetectBoxCollider(Vector2 playerPos, Vector2 playerSize, Vector2 ingredientPos, Vector2 ingredientSize)
        {
            float distanceX = Math.Abs(playerPos.X - ingredientPos.X);
            float distanceY = Math.Abs(playerPos.Y - ingredientPos.Y);

            float halfWidthSum = playerSize.X / 2f + ingredientSize.X / 2f;
            float halfHeightSum = playerSize.Y / 2f + ingredientSize.Y / 2f;

            return distanceX <= halfWidthSum && distanceY <= halfHeightSum;
        }

        //private void OnChangePointsHandler(int currentPoints, int maxPoints)
        //{

        //}
    }
}
