﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public class Button
    {
        private Vector2 position;
        private string normalTexture;
        private string selectedTexture;
        private float scale;
        private bool isSelected;

        public Button NextButton { get; private set; }
        public Button PreviousButton { get; private set; }

        public Button(Vector2 position, float scale, string normalTexture, string selectedTexture)
        {
            Initialize(position, scale, normalTexture, selectedTexture);
        }

        public void Initialize(Vector2 position, float scale, string normalTexture, string selectedTexture)
        {
            this.position = position;
            this.scale = scale;
            this.normalTexture = normalTexture;
            this.selectedTexture = selectedTexture;
        }

        public void AssignButton(Button previousButton, Button nextButton)
        {
            PreviousButton = previousButton;
            NextButton = nextButton;
        }

        public void Unselect()
        {
            isSelected = false;
        }

        public void Select()
        {
            isSelected = true;
        }

        public void Render()
        {
            Engine.Draw(isSelected ? normalTexture : selectedTexture, position.X, position.Y, scale, scale);
        }
    }
}
