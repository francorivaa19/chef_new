﻿using System;
using System.Collections.Generic;
using System.Media;

namespace Game.Scripts
{
    public class Program 
    {
        #region fields

        private const int WIDTH_SCREEN = 500;
        private const int HEIGHT_SCREEN = 700;

        public static bool canQuit;

        #endregion fields

        private static void Main(string[] args)
        {
            Initialization();

            while (!canQuit)
            {
                if (Engine.GetKey(Keys.ESCAPE)) canQuit = true;

                Time.CalculateDeltaTime();

                Update();
                Render();
            }
        }

        private static void Initialization()
        {
            Engine.Initialize("The Best Game You Ever Played", WIDTH_SCREEN, HEIGHT_SCREEN);
            Time.Start();
            GameManager.Instance.Initialize();
        }

        private static void Update()
        {
            GameManager.Instance.Update();

            var lol = new Random();
            lol.Next(0, 3);
            
            //Engine.Debug($"se colisiona??? + ${CollisionDetectionController.DetectCircleCollider(player.Position, player.Radius, ingredient.Position, ingredient.Radius)}");
            //Engine.Debug($"se colisiona??? + ${DetectCircleWithBoxColliding(player.Position, player.Size, ingredient.Position, ingredient.Radius)}");
            
        }

        private static void Render()
        {
            Engine.Clear();
            GameManager.Instance.Render();
            Engine.Show();
        }
    }
}