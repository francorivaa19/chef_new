﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public class Ingredient
    {
        #region fields

        private Vector2 position;
        public Vector2 Position { get => position; set => position = value; }

        private float speed = 350f;
        private float scale;
        private float angle;
        private string texture;
        private Texture ingredientTexture;
        private Character player;
        public bool isPickedUp = false;
        public float RealHeight => ingredientTexture.Height * scale;
        public float RealWidth => ingredientTexture.Width * scale;

        #endregion fields

        public Vector2 Size
        {
            get
            {
                return new Vector2(RealWidth, RealHeight);
            }
            set
            {
                Size = value;
            }
        }

        public float Radius => RealHeight > RealWidth ? RealHeight / 2f : RealWidth / 2f;

        public Ingredient(Vector2 initialPosition, string texture, float scale, float angle, Character player)
        {
            Initialize(initialPosition, texture, scale, angle, player);
        }

        public void Initialize(Vector2 initialPosition, string texture, float scale, float angle, Character player)
        {
            position = initialPosition;
            this.texture = texture;
            this.scale = scale;
            this.angle = angle;
            this.player = player;
            ingredientTexture = Engine.GetTexture(texture);
        }

        public void Update()
        {
            position.Y += speed * Time.deltaTime;
            CheckCollision();
        }

        public void Render()
        {
            Engine.Draw(ingredientTexture, this.position.X, this.position.Y, scale, scale, angle, RealWidth / 2f, RealHeight / 2f);
        }

        private void CheckCollision()
        {
            if (CollisionDetectionController.DetectCircleCollider(player.Position, player.Radius, this.Position, this.Radius))
            {
                isPickedUp = true;
            }
            else
                isPickedUp = false;
        }
    }
}
