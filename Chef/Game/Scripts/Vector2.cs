﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public struct Vector2
    {
        private float x;
        private float y;

        static public Vector2 Left => new Vector2(-1f, 0f);
        static public Vector2 Right => new Vector2(1f, 0f);
        static public Vector2 Down => new Vector2(0f, -1f);
        static public Vector2 Up => new Vector2(0f, 1f);
        static public Vector2 Zero => new Vector2(0f, 0f);

        public float X { get => x; set => x = value; }
        public float Y { get => y; set => y = value; }

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2 operator + (Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x + vector2.x, vector2.y + vector2.y);
        }

        public static Vector2 operator - (Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x - vector2.x, vector2.y - vector2.y);
        }

        public static Vector2 operator * (Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x * vector2.x, vector2.y * vector2.y);
        }

        public static Vector2 operator / (Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x / vector2.x, vector2.y / vector2.y);
        }

        public static bool operator == (Vector2 vector1, Vector2 vector2)
        {
            return (vector1.x == vector2.x && vector1.y == vector2.y);
        }

        public static bool operator != (Vector2 vector1, Vector2 vector2)
        {
            return (vector1.x != vector2.x || vector1.y != vector2.y);
        }

        public override bool Equals(object obj)
        {
            return obj is Vector2 vector &&
                   x == vector.x &&
                   y == vector.y &&
                   X == vector.X &&
                   Y == vector.Y;
        }

        public override int GetHashCode()
        {
            int hashCode = -624234986;
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + y.GetHashCode();
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}
