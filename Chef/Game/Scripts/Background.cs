﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Scripts
{
    public class Background
    {
        #region fields

        private float x;
        private float y;
        private float scale;
        private float angle;
        private string texture;
        private Texture backgroundTexture;
        public float RealHeight => backgroundTexture.Height * scale;
        public float RealWidth => backgroundTexture.Width * scale;

        #endregion fields

        public Background(float initialX, float initialY, string texturePath, float scale, float angle)
        {
            this.x = initialX;
            this.y = initialY;
            this.texture = texturePath;
            this.scale = scale;
            this.angle = angle;

            backgroundTexture = Engine.GetTexture(texturePath);
        }

        public void Update()
        {
            Render();
        }

        public void Render()
        {
            Engine.Draw(backgroundTexture, x, y, scale, scale, angle, RealHeight / 2f, RealWidth / 2f);
        }
    }
}
